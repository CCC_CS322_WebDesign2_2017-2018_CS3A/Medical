// for refresh

$(document).ready(function(){
    $(this).scrollTop(0);
});

// highlights

$(document).ready(function() {
	$(".icons").on("click", function(){
		$("nav ul").toggleClass("showing");
	});
});


$(document).ready(function() {
	$("#regs").on("click", function(){
		$("nav ul li a").removeClass("active");
		$("#regs").addClass("active");
	});
});

$(document).ready(function() {
	$(".homebutton").on("click", function(){
		$("nav ul li a").removeClass("active");
		$("#homes").addClass("active");
	});
});


$(document).ready(function() {
	$("nav ul li a").on("click", function(){
		$("nav ul li a").removeClass("active");
		$(this).addClass("active");
	});
});

// home icon

$(document).ready(function() {
	$(".shopnow").on("click", function(){
		$("nav ul li a").removeClass("active");
	});
});

// nav

$(window).on("scroll", function(){
	if($(window).scrollTop()) {
		$("nav").addClass("black");
	}
	else{
		$("nav").removeClass("black");
		$("nav ul li a").removeClass("active");
		$("#homes").addClass("active");
	}
	
	if($(window).scrollTop()) {
		$(".icons").addClass("black");
	}
	else{
		$(".icons").removeClass("black");
		$("nav ul li a").removeClass("active");
		$("#homes").addClass("active");
	}
});

// user

$(document).ready(function() {
	$(".shopnow").on("click", function(){
		if($(".user").css("display") === "none") {
		$(".intro").css("visibility", "visible");
		$(".contentLog").css("visibility", "visible");
		
	}
	});
});

$(document).ready(function() {
	$(".signs").on("click", function(){
		if($(".user").css("display") === "none") {
		$(".intro").css("visibility", "visible");
		$(".contentLog").css("visibility", "visible");
		
	}
	});
});


// modal disappear
$(document).ready(function() {
	$("#closebtn").on("click", function(){
		$(".intro").css("visibility", "hidden");
	});
});


//Scroll

$(document).ready(function(){
		  // Add smooth scrolling to all links
		  $(".navs").on('click', function(event) {

			// Make sure this.hash has a value before overriding default behavior
			if (this.hash !== "") {
			  // Prevent default anchor click behavior
			  event.preventDefault();

			  // Store hash
			  var hash = this.hash;

			  // Using jQuery's animate() method to add smooth page scroll
			  // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			  $('html, body').animate({
				scrollTop: $(hash).offset().top
			  }, 800, function(){
		   
				// Add hash (#) to URL when done scrolling (default click behavior)
				window.location.hash = hash;
			  });
			} // End if
		  });
		});
		
// Modal
var modal = document.getElementById('modal-wrapper');
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


