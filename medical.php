<?php include('server.php') ?>

<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>  <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>  <html class="no-js lt-ie9"> <![endif]-->
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name = "description" content = "Final Project"/>
		<meta http-equiv = "author" content = "Michaela L. Cruz, BSCS 3A"/>
		<title>Online Medical Supply Merchandise</title>
		
		<!-- css -->
		<link rel = "stylesheet" href = "css/style.css" type = "text/css"/>
		<link rel = "stylesheet" href = "css/style2.css" type = "text/css"/>
		<link rel = "stylesheet" href = "css/style3.css" type = "text/css"/>
		
		
		<!-- bootstrap -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">      
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		
		<link href="themes/css/bootstrappage.css" rel="stylesheet"/>
		
		<!-- global styles -->
		<link href="themes/css/flexslider.css" rel="stylesheet"/>
		<link href="themes/css/main.css" rel="stylesheet"/>

		<!-- scripts -->
		<script src="themes/js/jquery-1.7.2.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>				
		<script src="themes/js/superfish.js"></script>	
		<script src="themes/js/jquery.scrolltotop.js"></script>
		
		<script src = "js/jquery-3.2.1.min.js"></script>
		<script src = "js/jquery-3.3.1.min.js"></script>
		<script src = "js/jquery-3.2.1.js"></script>
		<script src = "js/script.js"></script>
		
	</head>
	<body>
	 <?php
		  if (isset($_GET['logout'])) {
			session_destroy();
			unset($_SESSION['username']);
			header("location: clothes.php");
		  }
	?>
	
	<?php  if (isset($_SESSION['username'])) : ?>
		<script type = "text/javascript">
		$(document).ready(function() {
			if($(".user").css("display") === "none") {
				$(".user").css("display", "inline-block");
				$(".user").text("<?php echo $_SESSION['username']; ?>");
				$(".logouts").css("display", "inline-block");
				$(".regiss").css("display", "none");
				$(".signs").css("display", "none");
			}
		});
		</script>
    	
    <?php endif ?>
	
	
		<section class = "intro">
		
			<div class = "innerLog">
				<div class = "contentLog">
					<form method="post" action="medical.php">
					<img src = "img/huwi.png"/ class ="photo" >
					<p class = "sometext">Log on to experience more!</p>
					<?php include('errors.php'); ?>
					<input type = "text" name = "username" placeholder = "Username" class = "input1"/>
					<input type = "password" name = "password" placeholder = "Password" class = "input2"/>
					<button id = "loginbtn" type="submit" class="btnss" name="login_user">Login</button>
					<a id = "#closebtn" href = "" class ="btons">Close</a>
					<a href = "register.php" class ="join">Don't have an account? | Join us now!</a>
					</form>	
				</div>
			</div>
			
		</section>
		
		<div class = "wrapper" id ="home">
			<header>
				<nav>
					<div class = "icons">
						<img src = "img/ham.png"/>
					</div>
					<div class = "logo">
						<a href = "#home" class = "navs homebutton"><img src = "img/huwi.png"/></a>
					</div>
					<div class = "menu">
						<ul>
							<li><a id = "homes" class = "active navs" href = "#home">Home</a></li>
							<li><a href = "#about" class = "navs">About</a></li>
							<li><a href = "register.php" class = "navs regiss">Register |</a></li>
							<li><a href = "#nows" class = "navs signs">Sign in |</a></li>
							<li><a href = "#" class = "navs">Cart(0)|</a></li>
							<li class = "user">User</li>
							<li><p> <a class ="logouts" href="clothes.php?logout='1'" style="display: none; color: red;">logout</a> </p></li>
						</ul>
					</div>
				</nav>
				<div class = "button-holder">
					<a id = "shopbtn" href="#nows" class= "shopnow navs">Shop now!</a>
				</div>
				
			
			</header>
		</div>
		
							<div class="span12" >
								<h4 class="title" id = "nows">
									<span class="pull-left"><span class="text"><span class="line">Featured <strong>Products</strong></span></span></span>
									<span class="pull-right">
										<a class="left button" href="#myCarousel" data-slide="prev"></a><a class="right button" href="#myCarousel" data-slide="next"></a>
									</span>
								</h4>
								<div id="myCarousel" class="myCarousel carousel slide">
									<div class="carousel-inner">
										<div class="active item">
											<ul class="thumbnails">												
												<li class="span3">
													<div class="product-box">
														<span class="sale_tag"></span>
														<p><a href="details.php?product=1"><img src="img/1.jpg" alt="" /></a></p>
														<a href="details.php?product=1" class="title">Hot rubber compressed bag</a><br/>
														<a href="details.php?	=1" class="category">Online Medical Supply Merchandise</a>
														<p class="price">PHP 189.00</p>
													</div>
												</li>
												<li class="span3">
													<div class="product-box">
														<span class="sale_tag"></span>
														<p><a href="details.php?product=2"><img src="img/2.jpg" alt="" /></a></p>
														<a href="details.php?product=2" class="title">Blood pressure equipment</a><br/>
														<a href="details.php?product=2roducts.html" class="category">Online Medical Supply Merchandise</a>
														<p class="price">PHP 389.00</p>
													</div>
												</li>
												<li class="span3">
													<div class="product-box">
														<p><a href="details.php?product=3"><img src="img/3.jpg" alt="" /></a></p>
														<a href="details.php?product=3" class="title">Nebulizer</a><br/>
														<a href="details.php?product=3" class="category">Online Medical Supply Merchandise</a>
														<p class="price">PHP 679.00</p>
													</div>
												</li>
												<li class="span3">
													<div class="product-box">
														<p><a href="details.php?product=4"><img src="img/4.jpg" alt="" /></a></p>
														<a href="details.php?product=4" class="title">Adult walker</a><br/>
														<a href="details.php?product=4" class="category">Online Medical Supply Merchandise<a>
														<p class="price">PHP 1570.00</p>
													</div>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul class="thumbnails">
												<li class="span3">
													<div class="product-box">
														<p><a href="details.php?product=5"><img src="img/5.jpg" alt="" /></a></p>
														<a href="details.php?product=5" class="title">First aid kit</a><br/>
														<a href="details.php?product=5" class="category">Online Medical Supply Merchandise</a>
														<p class="price">PHP 3,051.00</p>
													</div>
												</li>
																																
											</ul>
										</div>
									</div>							
								</div>
							</div>
		<div class="span12">
		<section id="footer-bar">
				<div class="row">
					<div class="span4">
						<h4 id = "about">About us</h4>
						<ul class="nav">
							<li>This is a Online Medical Supply Merchandise.</li>  
							<li>Submitted and Presented to: </li>
							<li>Mr. Jethro Gamad </li>								
						</ul>					
					</div>
					<div class="span5">
						<h4 id = "contact">Contact Me</h4>
						<ul class="nav">
							<li>Phone: 09051332227</li>  
							<li>Email: michacassie69@gmail.com</li>
														
						</ul>
					</div>	
				</div>	
			</section>
			<section id="copyright">
			<span>Copyright 2018&reg kitkat productions|| All rights reserved.</span>
			</section>
		</div>
		
		<!-- modal -->
		
	</body>
</html>