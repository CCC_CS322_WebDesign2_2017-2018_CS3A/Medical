-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2018 at 09:41 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `registration`
--

-- --------------------------------------------------------

--
-- Table structure for table `itemdsc`
--

CREATE TABLE `itemdsc` (
  `pcode` int(11) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `rpoints` int(11) NOT NULL,
  `imgs` varchar(255) NOT NULL,
  `itemname` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itemdsc`
--

INSERT INTO `itemdsc` (`pcode`, `brand`, `qty`, `rpoints`, `imgs`, `itemname`, `price`) VALUES
(1, 'Huwi', 20, 20, '1.jpg', 'Hot Compress Bag', 1800),
(2, 'Hwui', 20, 100, '2.jpg', 'Blood pressure equipment' 3800),
(3, 'Huwi', 30, 70, '3.jpg', 'Nebulizer', 700),
(4, 'Huwi', 30, 40, '4.jpg', 'Adult Walker', 1700),
(5, 'Huwi', 50, 100, '5.jpg', 'First Aid Kit', 3100),;

-- --------------------------------------------------------

--
-- Table structure for table `tempcart`
--

CREATE TABLE `tempcart` (
  `idcart` int(11) NOT NULL,
  `prodcart` varchar(255) NOT NULL,
  `imgcart` varchar(255) NOT NULL,
  `qtycart` int(11) NOT NULL,
  `unitprice` int(11) NOT NULL,
  `totalprice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(1, 'wew', 'adrianpabs14@gmail.com', '3847820138564525205299f1f444c5ec'),
(2, 'wewss', 'wewwssss@gmail.com', '3847820138564525205299f1f444c5ec'),
(3, 'kitkat014', 'adrianpabsss14@gmail.com', '589aa5777f8d03c8c30ed291bd3ef487'),
(4, 'jhondrielmanyak', 'manyaksiako@gmail.com', 'ffa044f5d18974a3264c23a5e1e06217'),
(5, 'wewewew', 'weweweew@gmail.com', '3847820138564525205299f1f444c5ec'),
(6, 'larabel', 'wewwew@gmail.com', '3847820138564525205299f1f444c5ec'),
(7, 'laratots', 'admin@gmail.com', '3847820138564525205299f1f444c5ec'),
(8, 'weweweewe', 'eweweewewew@gmail.com', '3847820138564525205299f1f444c5ec'),
(9, 'wewewewew', 'wewwewewwewew@gmail.com', '3847820138564525205299f1f444c5ec'),
(10, 'wewwewwewe', 'wewewewewew@gmail.com', 'a16101a30a25c4d4e79c971eb412c2b4'),
(11, 'honeybabe', 'honeybabe@gmail.com', '589aa5777f8d03c8c30ed291bd3ef487'),
(12, 'carlo', 'datingaling@gmail.com', 'ffa044f5d18974a3264c23a5e1e06217'),
(13, 'waw', 'waw@gmail.com', '7dd87e1bac147f619208ad97426ef9df'),
(14, 'AdrianPabs', 'adrianpabssssss14@gmail.com', '589aa5777f8d03c8c30ed291bd3ef487'),
(15, 'manyakjosue', 'manyakjosue@gmail.com', 'a824bda3c0e260691a9f18d1c3c13574'),
(16, 'manyakpabe', 'manyakpabe@gmail.com', '25f9e794323b453885f5181f1b624d0b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `itemdsc`
--
ALTER TABLE `itemdsc`
  ADD PRIMARY KEY (`pcode`);

--
-- Indexes for table `tempcart`
--
ALTER TABLE `tempcart`
  ADD PRIMARY KEY (`idcart`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `itemdsc`
--
ALTER TABLE `itemdsc`
  MODIFY `pcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tempcart`
--
ALTER TABLE `tempcart`
  MODIFY `idcart` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
