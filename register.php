<?php include('server.php') ?>

<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>  <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>  <html class="no-js lt-ie9"> <![endif]-->

<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name = "description" content = "Final Project"/>
		<meta http-equiv = "author" content = "Michaela L. Cruz, BSCS 3A"/>
		<title>Online Medical Supply Merchandise</title>
		
		<!-- css -->
		<link rel = "stylesheet" href = "css/style.css" type = "text/css"/>
		<link rel = "stylesheet" href = "css/style2.css" type = "text/css"/>
		<link rel = "stylesheet" href = "css/style3.css" type = "text/css"/>
		
		
		<!-- bootstrap -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">      
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		
		<link href="themes/css/bootstrappage.css" rel="stylesheet"/>
		
		<!-- global styles -->
		<link href="themes/css/flexslider.css" rel="stylesheet"/>
		<link href="themes/css/main.css" rel="stylesheet"/>

		<!-- scripts -->
		<script src="themes/js/jquery-1.7.2.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>				
		<script src="themes/js/superfish.js"></script>	
		<script src="themes/js/jquery.scrolltotop.js"></script>
		
		<script src = "js/jquery-3.2.1.min.js"></script>
		<script src = "js/jquery-3.3.1.min.js"></script>
		<script src = "js/jquery-3.2.1.js"></script>
		<script src = "js/scriptreg.js"></script>
		
	</head>
	<body>
		<div class = "wrapper" id ="reg">
			<header>
				<nav>
					<div class = "icons">
						<img src = "img/ham.png"/>
					</div>
					<div class = "logo">
						<a href = "medical.php" class = "navs homebutton"><img src = "img/pabelouswt.png"/></a>
					</div>
					<div class = "menu">
						<ul>
							<li><a id = "homes" class = "navs" href = "clothes.php">Home</a></li>
							<li><a href = "#about" class = "navs">About</a></li>
							<li><a id = "regs" href = "#reg" class = "active navs">Register |</a></li>
							<li><a href = "#" class = "navs">Sign in |</a></li>
							<li><a href = "#" class = "navs">Cart (0)|</a></li>
							<li class = "user">User</li>
						</ul>
					</div>
				</nav>
				
				<div class ="reg-holder">
				<div class="regi">
								
						<h4 class="title"><span class="text"><strong>Register</strong> Form</span></h4>
						<form action="register.php" method="post" class="form-stacked">
							<?php include('errors.php'); ?>
							<fieldset>
								<div class="control-group">
									<label class="control-label">Username</label>
									<div class="controls">
										<input type="text" name = "username" placeholder="Enter your username" class="input-xlarge">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Email address:</label>
									<div class="controls">
										<input type="email" name = "email" placeholder="Enter your email" class="input-xlarge">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Password:</label>
									<div class="controls">
										<input type="password" name = "password_1" placeholder="Enter your password" class="input-xlarge">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Confirm Password:</label>
									<div class="controls">
										<input type="password" name = "password_2" placeholder="Re-enter password" class="input-xlarge">
									</div>
								</div>									
								<div class="control-group">
									<p>You're now registered!</p>
								</div>
								<hr>
								<div class="actions"><input tabindex="9" class="btn btn-inverse large" type="submit" name = "reg_user" value="Create your account"></div>
								<a class = "join" href = "clothes.php">Already have an account? Log in now!</a>
							</fieldset>
						</form>
				
				</div>
				</div>
			</header>
			
		</div>
		
		
		<div class="span12">
		<section id="footer-bar">
				<div class="row">
					<div class="span4">
						<h4 id = "about">About us</h4>
						<ul class="nav">
							<li>This is a Online Medical Supply Merchandise.</li>  
							<li>Submitted and Presented to: </li>
							<li>Mr. Jethro Gamad </li>								
						</ul>					
					</div>
					<div class="span5">
						<h4 id = "contact">Contact Me</h4>
						<ul class="nav">
							<li>Phone: 09051332227</li>  
							<li>Email: michacassie69@gmail.com</li>
														
						</ul>
					</div>	
				</div>	
			</section>
			<section id="copyright">
			<span>Copyright 2018&reg kitkat productions|| All rights reserved.</span>
			</section>
		</div>
	</body>
</html>