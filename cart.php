<?php include('server.php') ?>

<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>  <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>  <html class="no-js lt-ie9"> <![endif]-->
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name = "description" content = "Final Project"/>
		<meta http-equiv = "author" content = "Michaela L. Cruz, BSCS 3A"/>
		<title>Online Medical Supply Merchandise</title>
		
		<!-- css -->
		<link rel = "stylesheet" href = "css/style.css" type = "text/css"/>
		<link rel = "stylesheet" href = "css/style2.css" type = "text/css"/>
		<link rel = "stylesheet" href = "css/style3.css" type = "text/css"/>
		
		
		<!-- bootstrap -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">      
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">		
		<link href="themes/css/bootstrappage.css" rel="stylesheet"/>
		
		<!-- global styles -->
		<link href="themes/css/main.css" rel="stylesheet"/>
		<link href="themes/css/jquery.fancybox.css" rel="stylesheet"/>
				
		<!-- scripts -->
		<script src="themes/js/jquery-1.7.2.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>				
		<script src="themes/js/superfish.js"></script>	
		<script src="themes/js/jquery.scrolltotop.js"></script>
		<script src="themes/js/jquery.fancybox.js"></script>
		<!--[if lt IE 9]>			
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->
		
		<script src = "js/jquery-3.2.1.min.js"></script>
		<script src = "js/jquery-3.3.1.min.js"></script>
		<script src = "js/jquery-3.2.1.js"></script>
		<script src = "js/script.js"></script>
		
		<?php
		  if (isset($_GET['logout'])) {
			session_destroy();
			unset($_SESSION['username']);
			header("location: medical Supply.php");
		  }
	?>
	
	<?php  if (isset($_SESSION['username'])) : ?>
		<script type = "text/javascript">
		$(document).ready(function() {
			if($(".user").css("display") === "none") {
				$(".user").css("display", "inline-block");
				$(".user").text("<?php echo $_SESSION['username']; ?>");
				$(".logouts").css("display", "inline-block");
				$(".regiss").css("display", "none");
				$(".signs").css("display", "none");
			}
		});
		</script>
    	
    <?php endif ?>
		
	</head>
	<body>
	
		<header>
				<nav>
					<div class = "icons">
						<img src = "img/ham.png"/>
					</div>
					<div class = "logo">
						<a href = "#home" class = "navs homebutton"><img src = "img/huwi.png"/></a>
					</div>
					<div class = "menu">
						<ul>
							<li><a id = "homes" class = "navs" href = "#home">Home</a></li>
							<li><a href = "#about" class = "navs">About</a></li>
							<li><a href = "register.php" class = "navs regiss">Register |</a></li>
							<li><a href = "#nows" class = "navs signs">Sign in |</a></li>
							<li><a href = "#" class = "active navs">Cart (0)|</a></li>
							<li class = "user">User</li>
							<li><p> <a class ="logouts" href="clothes.php?logout='1'" style="display: none; color: red;">logout</a> </p></li>
						</ul>
					</div>
				</nav>
				
			
			</header>
	</body>
	
</html>
	