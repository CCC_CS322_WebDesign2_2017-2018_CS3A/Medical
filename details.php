<?php include('server.php') ?>

<?php
$db = mysqli_connect('localhost', 'root', '', 'registration');
if ($_GET['product'] !== null) {
    $id = (int) $_GET['product'];
    $result = mysqli_query($db, "SELECT * FROM itemdsc WHERE pcode='$id'");
	$row = mysqli_fetch_array($result);
}
?>
	
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>  <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>  <html class="no-js lt-ie9"> <![endif]	-->
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name = "description" content = "Final Project"/>
		<meta http-equiv = "author" content = "Michaela L. Cruz, BSCS 3A"/>
		<title>Online Medical Supply Merchamdise</title>
		
		<!-- css -->
		<link rel = "stylesheet" href = "css/style.css" type = "text/css"/>
		<link rel = "stylesheet" href = "css/style2.css" type = "text/css"/>
		<link rel = "stylesheet" href = "css/style3.css" type = "text/css"/>
		
		
		<!-- bootstrap -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">      
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">		
		<link href="themes/css/bootstrappage.css" rel="stylesheet"/>
		
		<!-- global styles -->
		<link href="themes/css/main.css" rel="stylesheet"/>
		<link href="themes/css/jquery.fancybox.css" rel="stylesheet"/>
				
		<!-- scripts -->
		<script src="themes/js/jquery-1.7.2.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>				
		<script src="themes/js/superfish.js"></script>	
		<script src="themes/js/jquery.scrolltotop.js"></script>
		<script src="themes/js/jquery.fancybox.js"></script>
		<!--[if lt IE 9]>			
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->
		
		<script src = "js/jquery-3.2.1.min.js"></script>
		<script src = "js/jquery-3.3.1.min.js"></script>
		<script src = "js/jquery-3.2.1.js"></script>
		<script src = "js/script.js"></script>
		
		<?php
		  if (isset($_GET['logout'])) {
			session_destroy();
			unset($_SESSION['username']);
			header("location: clothes.php");
		  }
	?>
	
	<?php  if (isset($_SESSION['username'])) : ?>
		<script type = "text/javascript">
		$(document).ready(function() {
			if($(".user").css("display") === "none") {
				$(".user").css("display", "inline-block");
				$(".user").text("<?php echo $_SESSION['username']; ?>");
				$(".logouts").css("display", "inline-block");
				$(".regiss").css("display", "none");
				$(".signs").css("display", "none");
			}
		});
		</script>
    	
    <?php endif ?>
		
	</head>
	<body>
			<header>
				<nav>
					<div class = "icons">
						<img src = "img/ham.png"/>
					</div>
					<div class = "logo">
						<a href = "#home" class = "navs homebutton"><img src = "img/huwi.png"/></a>
					</div>
					<div class = "menu">
						<ul>
							<li><a id = "homes" class = "navs" href = "medical.php">Home</a></li>
							<li><a href = "#about" class = "navs">About</a></li>
							<li><a href = "register.php" class = "navs regiss">Register |</a></li>
							<li><a href = "#nows" class = "navs signs">Sign in |</a></li>
							<li><a href = "#" class = "navs">Cart (0)|</a></li>
							<li class = "user">User</li>
							<li><p> <a class ="logouts" href="clothes.php?logout='1'" style="display: none; color: red;">logout</a> </p></li>
						</ul>
					</div>
				</nav>
			<section class="header_text sub">
			
				<h4 style = "visibility: hidden"><span>Product Detail</span></h4>
			</section>
			<section class="main-content" style = "background: rgba(770,770,770,0.8); padding-top: 4vh;">				
				<div class="row">						
					<div class="span9">
						<div class="row" style = "padding-bottom: 4vh;">
							<div class="span4">
								<img alt="" src="themes/images/ladies/<?php echo $row[4] ?>">										
								
							</div>
							<div class="span5">
								<address>
									<strong>Brand:</strong> <span><?php echo $row[1] ?></span><br>
									<strong>Product Code:</strong> <span>000<?php echo $row[0] ?></span><br>
									<strong>Reward Points:</strong> <span><?php echo $row[3] ?></span><br>
									<strong>Availability:</strong> <span><?php echo $row[2] ?> pcs.</span><br>								
								</address>									
								<h4><strong>Price: PHP <?php echo $row[6] ?>. 00</strong></h4>
							</div>
							<div class="span5">
								<form class="form-inline">
									
									<p>&nbsp;</p>
									<label>Qty:</label>
									<input type="text" class="span1" placeholder="1">
									<button class="btn btn-inverse" type="submit">Add to cart</button>
								</form>
							</div>							
						</div>
						<div class="row">
							<div class="span9">
								<ul class="nav nav-tabs" id="myTab">
									<li class="active" style = "padding-left: 2vw;"><a href="#homes">Description</a></li>
									
								</ul>							 
								<div class="tab-content">
									<div class="tab-pane active" id="homes" style = "color: black; padding-left: 3vw;">Product name: <?php echo $row[5] ?></div>
									<div class="tab-pane" id="profile">
										
									</div>
								</div>							
							</div>	
						</div>
					</div>
				</div>
			</section>
				
			</header>
			<div class="span12">
		<section id="footer-bar">
				<div class="row">
					<div class="span4">
						<h4 id = "about">About us</h4>
						<ul class="nav">
							<li>This is a Online Medical Supply Merchandise.</li>  
							<li>Submitted and Presented to: </li>
							<li>Mr. Jethro Gamad </li>								
						</ul>					
					</div>
					<div class="span5">
						<h4 id = "contact">Contact Me</h4>
						<ul class="nav">
							<li>Phone: 09051332227</li>  
							<li>Email: michacassie69@gmail.com</li>
														
						</ul>
					</div>	
				</div>	
			</section>
			<section id="copyright">
			<span>Copyright 2018&reg kitkat productions|| All rights reserved.</span>
			</section>
		</div>
		
	</body>
	
</html>